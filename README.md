# NTP LED Clock

4 digit LED clock software for the ESP8266 WiFi system on chip that sets itself using network time protocol from Internet time servers.   Automatically adjusts for daylight savings time changes if enabled.

Based on espNTPdigitalClock.ino authored by Nigel from: http://www.esp8266.com/viewtopic.php?f=29&t=3247

Supports Adafruit HT16K33 LED backpacks and Adafruit 4 digit  14 and 7 segment LED modules.

## Installation

First install Arduino IDE from https://www.arduino.cc/en/Main/Software, version 1.8.12 is known to work.

Then add ESP8266 support to the Arduino IDE program.   In File-> Preferences, paste the following into the board manager URL box: http://arduino.esp8266.com/stable/package_esp8266com_index.json

Then select your ESP8266 board in Tools->Board->Board Manager. Version 2.6.3 is known to work.

The following libraries are required to be added to Arduino:

Copy the Time-Master library from https://github.com/PaulStoffregen/Time  to /home/user/Arduino/Libraries.
Version  b9be1cb63893e54c73cccc5a54f5606ed2295035, dated Feb 11, 2020 is known to work.

 Install the following through Tools->Manage Libraries
 *    Adafruit-GFX-Library-master version 1.75 (known to work)
 *    Adafruit_LED_Backpack-master version 1.16 (known to work)

WiFi:  Rename password.h.example to password.h and edit it to add your WiFi access point ssid and password and then save.
Otherwise you will get a build error due to a missin password.h file.
