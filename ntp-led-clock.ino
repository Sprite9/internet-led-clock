/* NTP LED Clock
 *
 *  Based on espNTPdigitalClock.ino authored by Nigel from: http://www.esp8266.com/viewtopic.php?f=29&t=3247
 *
 *  Support Adafruit HT16K33 LED Backpacks
 *  Adafruit:  14 and 7 segment LEDs currently working, (Future: 8x8)
 *
 *  Requires the ESP8266 board. To add, in File-> Preferences, paste the following into
 *  board manager URL box.  http://arduino.esp8266.com/stable/package_esp8266com_index.json
 *  then select board in Tools->Board->Board Manager
 *
 *  Requires the following libraries added to Arduino IDE (read further for version and installation info):
 *     Time-master, Adafruit-GFX-Library-master, Adafruit_LED_Backpack-master
 *
 *  Versions known to work together:
 *
 *    Arduino 1.8.12
 *
 *    esp8266 board in Board Manager, version 2.6.3
 *
 *  The following installed through Tools->Manage Libraries
 *    Adafruite-GFX-Library-master version 1.75
 *    Adafruit_LED_Backpack-master version 1.16
 *
 *  Copy the following to /home/user/Arduino/Libraries
 *    Time-Master https://github.com/PaulStoffregen/Time  b9be1cb63893e54c73cccc5a54f5606ed2295035    Feb 11, 2020
 *
 *  WiFi:  Rename password.h.example to password.h and edit it to add your WiFi access point ssid and password and then save.
 *
 *  Todo:
 *        Support a second display to allow two time zones to be displayed.
 *        Clean up 14 segment display options for 12/24 hour and UTC
 *
*/

#include <Wire.h>
#include <Ticker.h>
#include <WiFiUdp.h>
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <Adafruit_GFX.h>
#include <Adafruit_LEDBackpack.h>

extern "C"
{
#include "user_interface.h"  // Espressif interface file
#include "password.h" // WiFi password file
}

/* Select 14 or 7 segment display */
//#define DISPLAY_14
#define DISPLAY_7

// If DISPLAY_12_HOUR is not defined then the displa is in 24 hour mode
#define DISPLAY_12_HOUR

/*  Time Zone Offset */
#define timeZoneOffset (-7)      // Time zone offset ftom UTC in +/-hours

/* Daylight Savings */
#define DST_ENABLE
#define DST_START_MONTH 3
#define DST_START_SUNDAY 2     // 1 = first, 2= second, 3 = 3rd, 4 = 4th, 5 = last Sunday in month (last sunday not implemented)
#define DST_END_MONTH 11
#define DST_END_SUNDAY 1       // 1 = first, 2= second, 3 = 3rd, 4 = 4th, 5 = last Sunday in month (last sunday not implemented)
#define DST_START_END_HOUR (2 - timeZoneOffset)  // 2 a.m.

/* NTP time server pool see: http://support.ntp.org/bin/view/Servers/NTPPoolServers */
const char* ntpTimeServer0 = "0.north-america.pool.ntp.org";    //  Primary regional time server, could be your own local time server
const char* ntpTimeServer1 = "1.north-america.pool.ntp.org";   //  Secondary time server

#define UPDATESECS 86341UL  // update every 24 hours - 59 seconds to spread out ntp server load

time_t update;

bool debug_serial_flag = true;
bool colon = false;
bool enable_colon_flash = false;
bool NTP_initial_update_success = false;
bool test_dst_calculation = true;

// A UDP instance to let us send and receive packets over UDP
WiFiUDP             udp;

// Initalize 7 segment display
#ifdef DISPLAY_7
Adafruit_7segment   disp = Adafruit_7segment();
#endif

// Initialize 14 segment display
#ifdef DISPLAY_14
Adafruit_AlphaNum4 alpha4 = Adafruit_AlphaNum4();
#endif

//RTOS schedular callback initialize
Ticker              showTime;

unsigned int localPort = 2390;               // local port to listen for UDP packets

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

void setup()
{
    Serial.begin(115200);    // Initialize default serial port
    Wire.begin(4,5);         // Pin 4 (NodeMcu D2) = sda, Pin 4 (NodeMcu D1) = sdl
    delay(10);
#ifdef DISPLAY_7
    disp.begin(0x70);   // default display adress for the LED backpack
#endif
#ifdef DISPLAY_14
    alpha4.begin(0x70); // default display adress for the LED backpack
#endif
    noTime();           // set the 7/14 segment display to default

    Serial.println();
    Serial.println();

    // Start by connecting to a WiFi network
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, pass);
    WiFi.mode(WIFI_STA);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    WiFi.disconnect();
    showTime.attach(0.333,doDisplay);
    setNTPtime();
}

void loop()
{
    if(now() > update) setNTPtime();
    // Allow for testing and debugging if needed
    if(debug_serial_flag && (now() % 30UL == 0UL))
    {
        //printTime("Now ",now());
        //dstOffset(now());
        if(test_dst_calculation == true && NTP_initial_update_success == true)
        {
            testDST();
            test_dst_calculation = false;
        }
    }
    else debug_serial_flag = true;
    delay(900);
}


void doDisplay()
{
    int ttime;
    static int utcHour24, localHour12, localHour24 ;
    static int lastHour = -1, dst=0;

    if(NTP_initial_update_success == false) return;  // time has not been initialized

    utcHour24 = hour();

    // Check for DST in effect every hour
    if(lastHour != utcHour24)
    {
        lastHour = utcHour24;
        dst = dstOffset(now());

        // Apply DST and time zone and formats
        localHour24 = timeZoneOffset + dst + utcHour24;
        if(localHour24 > 23) localHour24 -= 24;
        if(localHour24 < 0) localHour24 += 24;
        localHour12 = localHour24;
        if(localHour12 > 12) localHour12 -= 12;
    }

#ifdef DISPLAY_7
#ifdef DISPLAY_12_HOUR
    disp.print(localHour12 * 100 + minute());
#else
    disp.print(localHour24 * 100 + minute());
#endif
#endif
#ifdef DISPLAY_14
    //ttime = utcHour24 * 100 + minute();
    ttime = localHour12 * 100 + minute();
#endif

#ifdef DISPLAY_7

    if(enable_colon_flash == true)
    {
        disp.drawColon(colon);
        if(colon == true)
        {
            colon = false;
        }
        else
        {
            colon = true;
        }
    }
    else disp.drawColon(true);
    disp.writeDisplay();
#endif
#ifdef DISPLAY_14
    alphaWrite(ttime);
    alpha4.writeDisplay();
#endif
}

void setNTPtime()
{
    enable_colon_flash = false;
    WiFi.begin(ssid, pass);
    WiFi.mode(WIFI_STA);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println();
    time_t epoch = 0UL;
    if((epoch =  getFromNTP(ntpTimeServer0)) != 0 || (epoch =  getFromNTP(ntpTimeServer1)) != 0) // get from time server
    {
        Serial.print("ntp epoch:");
        Serial.println(epoch);
        epoch = epoch - 2208988800UL;   //Adjust from NPT seconds (second since from 00:00 1 Jan 1900) to Unix (seconds from 00:00 1 Jan 1970)
        Serial.print("unix epoch:");
        Serial.println(epoch);
        setTime(epoch);  // Set local clock to UTC time
        update = now() +  UPDATESECS; // set next update time if successful
        enable_colon_flash = true;
        NTP_initial_update_success = true;
    }
    else
    {
        enable_colon_flash = false;
        update = now() + 30; // or try again in 30 seconds
    }
    WiFi.disconnect();
}

unsigned long getFromNTP(const char* server)
{
    udp.begin(localPort);
    sendNTPpacket(server);   // Send an NTP packet to a time server
    delay(1000);         // Wait to see if a reply is available
    int cb = udp.parsePacket();
    if (!cb)
    {
        Serial.println("no packet yet");
        return 0UL;
    }
    Serial.print("packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

    // The timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, extract the two words:

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // Combine the four bytes (two words) into a long integer
    // This is NTP time (seconds since Jan 1 1900):
    udp.stop();
    return (unsigned long) highWord << 16 | lowWord;
}

// send an NTP request to the time server at the given address
unsigned long sendNTPpacket(const char* server)
{
    Serial.print("sending NTP packet to ");
    Serial.println(server);
    // set all bytes in the buffer to 0
    memset(packetBuffer, 0, NTP_PACKET_SIZE);
    // Initialize values needed to form NTP request
    // (see URL above for details on the packets)
    packetBuffer[0] = 0b11100011;   // LI, Version, Mode
    packetBuffer[1] = 0;     // Stratum, or type of clock
    packetBuffer[2] = 6;     // Polling Interval
    packetBuffer[3] = 0xEC;  // Peer Clock Precision
    // 8 bytes of zero for Root Delay & Root Dispersion
    packetBuffer[12]  = 49;
    packetBuffer[13]  = 0x4E;
    packetBuffer[14]  = 49;
    packetBuffer[15]  = 52;

    // All NTP fields have been given values, now
    // you can send a packet requesting a timestamp:
    udp.beginPacket(server, 123); //NTP requests are to port 123
    udp.write(packetBuffer, NTP_PACKET_SIZE);
    udp.endPacket();
}

// Calculate and print to the console 14 years worth of DST start end dates
// to confirm the calculation is working
void testDST(void)
{
    time_t t = now();

    tmElements_t te;
    te.Month = DST_START_MONTH;
    te.Day =1;
    te.Hour = 0;
    te.Minute = 0;
    te.Second = 0;

    time_t dstStart,dstEnd;
    Serial.println("Testing DST calculation");
    Serial.print("Time zone offset: ");
    Serial.println(timeZoneOffset);
    Serial.println("Note that DST start end times are UTC times for the configured time zone offset.");
    int ii;
    for (ii=0; ii<14; ii++)
    {
        te.Year = ii + year(t) - 1970;
        dstStartEndDates(te,&dstStart,&dstEnd)  ;
    }
}

int dstOffset (time_t t) // Determine if Daylight Savings time is in effect
{

#ifndef  DST_ENABLE
    return (0);
#endif

    tmElements_t te;
    te.Year = year(t)-1970;
    te.Month = DST_START_MONTH;
    te.Day =1;
    te.Hour = 0;
    te.Minute = 0;
    te.Second = 0;

    time_t dstStart,dstEnd, current;

    Serial.print("Time zone offset: ");
    Serial.println(timeZoneOffset);
    printTime("Now (time in utc): ",now());
    Serial.println("Note that DST start end times are UTC times for the configured time zone offset.");

    dstStartEndDates(te,&dstStart,&dstEnd)  ;

    if (t>=dstStart && t<dstEnd)
    {
        Serial.println("dst in effect");
        return (1);
    }
    else
    {
        Serial.println("dst not in effect");
        return (0);  //NonDST
    }
}

// Calculate start and end dates for DST
void dstStartEndDates(tmElements_t tm,time_t *startDST,time_t *endDST)
{
    *startDST = makeTime(tm);
    *startDST = NthSunday(*startDST, DST_START_SUNDAY);
    *startDST += DST_START_END_HOUR * SECS_PER_HOUR;
    printTime("dst start time ",*startDST);
    tm.Month=DST_END_MONTH;
    *endDST = makeTime(tm);
    *endDST = NthSunday(*endDST, DST_END_SUNDAY);
    *endDST += DST_START_END_HOUR * SECS_PER_HOUR;
    printTime("dst end time ",*endDST);
}

// Not currently implemented or confirmed working.
time_t lastSunday(time_t t)
{
    t = nextSunday(t);  //Once, first Sunday
    printTime("nextSunday ",t);
    Serial.print("day = ");
    Serial.println(day(t));
    if(day(t) < 4) return t += 4 * SECS_PER_WEEK;
    else return t += 3 * SECS_PER_WEEK;
}

time_t NthSunday(time_t t, int nthSunday)
{
    //Special case if the 1st of the Month is a Sunday
    if(weekday(t) == 1)
    {
        if(nthSunday == 1) return t;
        else nthSunday --;
    }

    t = nextSunday(t);  //Once, first Sunday

    if(nthSunday == 1) return t;
    t = nextSunday(t);  //Second Sunday

    if(nthSunday == 2) return t;
    t = nextSunday(t);  //Third Sunday

    if(nthSunday == 3) return t;

}

void printTime(const char s[],time_t t)
{
    debug_serial_flag =false;
    Serial.print(s);
    printLeading0(hour(t));
    Serial.print(":");
    printLeading0(minute(t));
    Serial.print(":");
    printLeading0(second(t));
    Serial.print(" ");
    Serial.print(dayStr(weekday(t)));
    Serial.print(" ");
    Serial.print(day(t));
    Serial.print(" ");
    Serial.print(monthStr(month(t)));
    Serial.print(" ");
    Serial.println(year(t));
}

void printLeading0(int n)
{
    if(n < 10) Serial.print("0");
    Serial.print(n);
}

void noTime(void)
{
#ifdef DISPLAY_7
    Serial.println("init 7 seg");
    disp.drawColon(false);
    disp.print(00.42);
    disp.writeDisplay();
#endif

#ifdef DISPLAY_14
    alpha4.writeDigitAscii(0,'0');
    alpha4.writeDigitAscii(1,'0');
    alpha4.writeDigitAscii(2,'0');
    alpha4.writeDigitAscii(3,'2');
    alpha4.writeDisplay();
    alpha4.setBrightness(1);
#endif
}

#ifdef DISPLAY_14
void alphaWrite(int atime)
{
    unsigned char dat;

    dat = atime / 1000;
    alpha4.writeDigitAscii(0,dat+48);
    dat = (atime / 100) - (dat * 10);
    alpha4.writeDigitAscii(1,dat+48);
    dat = (atime / 10) - (atime / 100)*10;
    alpha4.writeDigitAscii(2,dat+48);
    dat = (atime) - (atime / 10)*10;
    alpha4.writeDigitAscii(3,dat+48);
    alpha4.writeDisplay();
}
#endif
